Source: php-phalcon
Section: php
Priority: optional
Maintainer: Debian PHP PECL Maintainers <team+php-pecl@tracker.debian.org>
Uploaders: Ondřej Surý <ondrej@debian.org>
Build-Depends: debhelper (>= 10~),
               dh-php (>= 5.7~),
               libtool-bin,
               php-all-dev (>= 2:95~)
Standards-Version: 4.5.1
Homepage: https://phalconphp.com/
Vcs-Git: https://salsa.debian.org/php-team/pecl/php-phalcon.git
Vcs-Browser: https://salsa.debian.org/php-team/pecl/php-phalcon

Package: php-phalcon
Architecture: all
Pre-Depends: php-common (>= 2:69~)
Depends: ucf,
         ${misc:Depends},
         ${pecl:Depends},
         ${php:Depends},
         ${shlibs:Depends}
Provides: php-phalcon5,
          ${php:Provides}
Breaks: php-phalcon5 (<< 5.8.0-4~)
Replaces: php-phalcon5 (<< 5.8.0-4~)
Description: full-stack PHP framework delivered as a C-extension
 Phalcon is an open source full stack framework for PHP, written as a
 C-extension. Phalcon is optimized for high performance. Its unique architecture
 allows the framework to always be memory resident, offering its functionality
 whenever it’s needed, without expensive file stats and file reads that
 traditional PHP frameworks employ.
 .
 This is empty package that depends on default PHP version.

Package: php-phalcon-all-dev
Architecture: all
Pre-Depends: php-common (>= 2:69~)
Depends: ucf,
         ${misc:Depends},
         ${pecl:Depends},
         ${php:Depends},
         ${shlibs:Depends}
Provides: php-phalcon5,
          ${php:Provides}
Breaks: php-phalcon5 (<< 5.8.0-4~)
Replaces: php-phalcon5 (<< 5.8.0-4~)
Description: full-stack PHP framework delivered as a C-extension
 Phalcon is an open source full stack framework for PHP, written as a
 C-extension. Phalcon is optimized for high performance. Its unique architecture
 allows the framework to always be memory resident, offering its functionality
 whenever it’s needed, without expensive file stats and file reads that
 traditional PHP frameworks employ.
 .
 This is empty package that depends on all PHP versions.

Package: php8.4-phalcon
Architecture: any
Pre-Depends: php-common (>= 2:69~)
Depends: ucf,
         ${misc:Depends},
         ${pecl:Depends},
         ${php:Depends},
         ${shlibs:Depends}
Provides: php-phalcon5,
          ${php:Provides}
Breaks: php8.4-phalcon5 (<< 5.8.0-4~)
Replaces: php8.4-phalcon5 (<< 5.8.0-4~)
Description: full-stack PHP framework delivered as a C-extension
 Phalcon is an open source full stack framework for PHP, written as a
 C-extension. Phalcon is optimized for high performance. Its unique architecture
 allows the framework to always be memory resident, offering its functionality
 whenever it’s needed, without expensive file stats and file reads that
 traditional PHP frameworks employ.

Package: php8.3-phalcon
Architecture: any
Pre-Depends: php-common (>= 2:69~)
Depends: ucf,
         ${misc:Depends},
         ${pecl:Depends},
         ${php:Depends},
         ${shlibs:Depends}
Provides: php-phalcon5,
          ${php:Provides}
Breaks: php8.3-phalcon5 (<< 5.8.0-4~)
Replaces: php8.3-phalcon5 (<< 5.8.0-4~)
Description: full-stack PHP framework delivered as a C-extension
 Phalcon is an open source full stack framework for PHP, written as a
 C-extension. Phalcon is optimized for high performance. Its unique architecture
 allows the framework to always be memory resident, offering its functionality
 whenever it’s needed, without expensive file stats and file reads that
 traditional PHP frameworks employ.

Package: php8.2-phalcon
Architecture: any
Pre-Depends: php-common (>= 2:69~)
Depends: ucf,
         ${misc:Depends},
         ${pecl:Depends},
         ${php:Depends},
         ${shlibs:Depends}
Provides: php-phalcon5,
          ${php:Provides}
Breaks: php8.2-phalcon5 (<< 5.8.0-4~)
Replaces: php8.2-phalcon5 (<< 5.8.0-4~)
Description: full-stack PHP framework delivered as a C-extension
 Phalcon is an open source full stack framework for PHP, written as a
 C-extension. Phalcon is optimized for high performance. Its unique architecture
 allows the framework to always be memory resident, offering its functionality
 whenever it’s needed, without expensive file stats and file reads that
 traditional PHP frameworks employ.

Package: php8.1-phalcon
Architecture: any
Pre-Depends: php-common (>= 2:69~)
Depends: ucf,
         ${misc:Depends},
         ${pecl:Depends},
         ${php:Depends},
         ${shlibs:Depends}
Provides: php-phalcon5,
          ${php:Provides}
Breaks: php8.1-phalcon5 (<< 5.8.0-4~)
Replaces: php8.1-phalcon5 (<< 5.8.0-4~)
Description: full-stack PHP framework delivered as a C-extension
 Phalcon is an open source full stack framework for PHP, written as a
 C-extension. Phalcon is optimized for high performance. Its unique architecture
 allows the framework to always be memory resident, offering its functionality
 whenever it’s needed, without expensive file stats and file reads that
 traditional PHP frameworks employ.

Package: php8.0-phalcon
Architecture: any
Pre-Depends: php-common (>= 2:69~)
Depends: ucf,
         ${misc:Depends},
         ${pecl:Depends},
         ${php:Depends},
         ${shlibs:Depends}
Provides: php-phalcon5,
          ${php:Provides}
Breaks: php8.0-phalcon5 (<< 5.8.0-4~)
Replaces: php8.0-phalcon5 (<< 5.8.0-4~)
Description: full-stack PHP framework delivered as a C-extension
 Phalcon is an open source full stack framework for PHP, written as a
 C-extension. Phalcon is optimized for high performance. Its unique architecture
 allows the framework to always be memory resident, offering its functionality
 whenever it’s needed, without expensive file stats and file reads that
 traditional PHP frameworks employ.

Package: php-phalcon5
Architecture: all
Depends: php-phalcon (>= 5.8.0-4~)
Description: full-stack PHP framework delivered as a C-extension (dummy)
 Phalcon is an open source full stack framework for PHP, written as a
 C-extension. Phalcon is optimized for high performance. Its unique architecture
 allows the framework to always be memory resident, offering its functionality
 whenever it’s needed, without expensive file stats and file reads that
 traditional PHP frameworks employ.
 .
 This is dummy dependency package and can be safely removed.

Package: php8.0-phalcon5
Architecture: all
Depends: php8.0-phalcon (>= 5.8.0-4~)
Description: full-stack PHP framework delivered as a C-extension (dummy)
 Phalcon is an open source full stack framework for PHP, written as a
 C-extension. Phalcon is optimized for high performance. Its unique architecture
 allows the framework to always be memory resident, offering its functionality
 whenever it’s needed, without expensive file stats and file reads that
 traditional PHP frameworks employ.
 .
 This is dummy dependency package and can be safely removed.

Package: php8.1-phalcon5
Architecture: all
Depends: php8.1-phalcon (>= 5.8.0-4~)
Description: full-stack PHP framework delivered as a C-extension (dummy)
 Phalcon is an open source full stack framework for PHP, written as a
 C-extension. Phalcon is optimized for high performance. Its unique architecture
 allows the framework to always be memory resident, offering its functionality
 whenever it’s needed, without expensive file stats and file reads that
 traditional PHP frameworks employ.
 .
 This is dummy dependency package and can be safely removed.

Package: php8.2-phalcon5
Architecture: all
Depends: php8.2-phalcon (>= 5.8.0-4~)
Description: full-stack PHP framework delivered as a C-extension (dummy)
 Phalcon is an open source full stack framework for PHP, written as a
 C-extension. Phalcon is optimized for high performance. Its unique architecture
 allows the framework to always be memory resident, offering its functionality
 whenever it’s needed, without expensive file stats and file reads that
 traditional PHP frameworks employ.
 .
 This is dummy dependency package and can be safely removed.

Package: php8.3-phalcon5
Architecture: all
Depends: php-phalcon (>= 5.8.0-4~)
Description: full-stack PHP framework delivered as a C-extension (dummy)
 Phalcon is an open source full stack framework for PHP, written as a
 C-extension. Phalcon is optimized for high performance. Its unique architecture
 allows the framework to always be memory resident, offering its functionality
 whenever it’s needed, without expensive file stats and file reads that
 traditional PHP frameworks employ.
 .
 This is dummy dependency package and can be safely removed.
